# User Stories Example

This project is for cra-template with Typscript template.

[Home](https://task-force-challenge-user-stories.netlify.app/)

## How to run this app.

- First you will need to clone the node app.
- Then you can use **yarn** or **npm** to install the necessary packages.
- Then you can use **yarn start** or **npm run start** to run the application.
