import { useState } from "react";
import Modal from "../common/Modal";
import "./index.scss";

const Analytics = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <div className="analytics_container">
      <div className="analytics_container__header">
        <p className="analytics_container__header__title">Analytics</p>
        <span
          className="analytics_container__header__action"
          onClick={() => setShowModal(true)}
        >
          Print
        </span>
      </div>

      <div className="analytics_container__body">
        <div className="analytics_container__body__statistic open_cases">
          <p className="analytics_container__body__statistic__title">
            Open Cases
          </p>
          <p className="analytics_container__body__statistic__date">
            31st August 2021
          </p>
          <p className="analytics_container__body__statistic__total">173</p>
        </div>

        <div className="analytics_container__body__statistic closed_cases">
          <p className="analytics_container__body__statistic__title">
            Closed Cases
          </p>
          <p className="analytics_container__body__statistic__date">
            31st August 2021
          </p>
          <p className="analytics_container__body__statistic__total">413</p>
        </div>

        <div className="analytics_container__body__statistic lawyers">
          <p className="analytics_container__body__statistic__title">Lawyers</p>
          <p className="analytics_container__body__statistic__date">
            31st August 2021
          </p>
          <p className="analytics_container__body__statistic__total">32</p>
        </div>

        <div className="analytics_container__body__statistic clients">
          <p className="analytics_container__body__statistic__title">Clients</p>
          <p className="analytics_container__body__statistic__date">
            31st August 2021
          </p>
          <p className="analytics_container__body__statistic__total">367</p>
        </div>
      </div>

      <Modal
        header="Export Data"
        showModal={showModal}
        setShowModal={setShowModal}
      >
        <div className="info">
          <input type="text" placeholder="Reason for printing" />
          <input type="submit" value="Submit And Print" />
        </div>
      </Modal>
    </div>
  );
};

export default Analytics;
