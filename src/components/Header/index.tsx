import "./index.scss";
import { useHistory } from "react-router-dom";

const Header = ({ role }) => {
  const { push } = useHistory();
  return (
    <div className="header_container">
      <div className="header_container__role">
        <p className="header_container__role__label">Role:</p>
        <p className="header_container__role__value">{role}</p>
      </div>
      <span className="header_container__logout" onClick={() => push("/login")}>
        LOGOUT
      </span>
    </div>
  );
};

export default Header;
