import { useState } from "react";
import "./index.scss";
import cases from "../../../constants/cases";
import Modal from "../../common/Modal";
const AssignedCases = () => {
  const [showModal, setShowModal] = useState(false);
  const [tasks, setTasks] = useState<Array<String>>([]);

  const handleChange = ({ target: { name, value } }) => {
    if (tasks.includes(value)) {
      setTasks(tasks.filter((t) => t !== value));
    } else setTasks([...tasks, value]);
  };

  console.log(tasks);

  return (
    <div className="assigned_cases_container">
      <div className="assigned_cases_container__header">
        <p className="assigned_cases_container__header__title">My Cases</p>
      </div>

      <div className="assigned_cases_container__body">
        <div
          className="assigned_cases_container__body__case"
          onClick={() => {
            setShowModal(true);
          }}
        >
          <div className="assigned_cases_container__body__case__id">
            {cases[0].id}
          </div>

          <div className="assigned_cases_container__body__case__title">
            {cases[0].title}
          </div>

          <div className="assigned_cases_container__body__case__description">
            {cases[0].description}
          </div>
        </div>
      </div>

      <Modal
        header="Assigned Case Details"
        showModal={showModal}
        setShowModal={setShowModal}
      >
        <div className="info">
          <p className="case_id">Case ID: {cases[0].id}</p>
          <p>Client: {cases[0].client}</p>
          <p>Issued Date: {cases[0].issuedDate}</p>
        </div>

        <div className="tasks">
          <p className="tasks_title">Tasks</p>
          <div className="task_checkbox">
            <input
              onChange={handleChange}
              type="checkbox"
              name="task1"
              value="task1"
              id="task1"
            />
            <label htmlFor="task1">Find evidence</label>
          </div>

          <div className="task_checkbox">
            <input
              onChange={handleChange}
              type="checkbox"
              name="task1"
              value="task2"
              id="task2"
            />
            <label htmlFor="task2">Apply for appeal</label>
          </div>

          <div className="task_checkbox">
            <input
              onChange={handleChange}
              type="checkbox"
              name="task1"
              value="task3"
              id="task3"
            />
            <label htmlFor="task3">Apply for appeal</label>
          </div>
        </div>

        <div className="actions">
          <span
            className={`${
              tasks.length < 3 ? "not-completed" : "create_invoice"
            }`}
          >
            Create Invoice
          </span>
        </div>
      </Modal>
    </div>
  );
};

export default AssignedCases;
