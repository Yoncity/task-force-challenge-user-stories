import { useState } from "react";
import "./index.scss";
import cases, { CaseModel } from "../../constants/cases";
import invoices, { InvoiceModel } from "../../constants/invoices";
import Modal from "../common/Modal";

const Cases = () => {
  const [tab, setTab] = useState(0);
  const [showModal, setShowModel] = useState(false);
  const [showInvoiceModal, setShowInvoiceModel] = useState(false);
  const [caseDetails, setCaseDetails] = useState<CaseModel>();
  const [invoiceDetails, setInvoiceDetails] = useState<InvoiceModel>();

  const showCaseDetails = (id) => {
    const c = cases.find((item) => item.id === id);
    setCaseDetails(c);
    setShowModel(true);
  };

  const showInvoiceDetails = (no) => {
    const c = invoices.find((item) => item.no === no);
    setInvoiceDetails(c);
    setShowInvoiceModel(true);
  };

  const showCases = () => {
    return (
      <div className="cases_container__body__all_cases">
        {cases.map((item) => (
          <div
            className="cases_container__body__all_cases__case"
            onClick={() => showCaseDetails(item.id)}
            key={item.id}
          >
            <p className="cases_container__body__all_cases__case__id">
              {item.id}
              {item.assigned && <span className="assigned">assigned</span>}
            </p>
            <p className="cases_container__body__all_cases__case__title">
              {item.title}
            </p>

            <p className="cases_container__body__all_cases__case__description">
              {item.description}
            </p>
          </div>
        ))}
      </div>
    );
  };

  const showInvoices = () => {
    return (
      <div className="cases_container__body__all_invoices">
        {invoices.map((item) => (
          <div
            className="cases_container__body__all_invoices__invoice"
            onClick={() => showInvoiceDetails(item.no)}
            key={item.no}
          >
            <p className="cases_container__body__all_invoices__invoice__no">
              Invoice No: {item.no}
              {item.approved && <span className="approved">approved</span>}
            </p>
            <p className="cases_container__body__all_invoices__invoice__case">
              Case ID: {item.caseID}
            </p>
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="cases_container">
      <div className="cases_container__header">
        <p
          className={`cases_container__header__title ${
            tab === 0 ? "active" : ""
          }`}
          onClick={() => setTab(0)}
        >
          All Cases
        </p>
        <p
          className={`cases_container__header__title ${tab ? "active" : ""}`}
          onClick={() => setTab(1)}
        >
          Invoices
        </p>
      </div>

      <div className="cases_container__body">
        {tab ? showInvoices() : showCases()}
      </div>
      {invoiceDetails && (
        <Modal
          header="Invoice Details"
          showModal={showInvoiceModal}
          setShowModal={setShowInvoiceModel}
        >
          <div className="info">
            <p className="case_id">Case ID: {invoiceDetails.caseID}</p>
            <p>Amount: ${invoiceDetails.amount}</p>
            <p>Issued By: {invoiceDetails.issueDate}</p>
            <p>Issued Date: {invoiceDetails.issuedBy}</p>
          </div>

          <div className="actions">
            <span
              className={
                invoiceDetails.approved ? "revoke_approval" : "approve"
              }
            >
              {invoiceDetails.approved ? "Revoke Approval" : "Approve"}
            </span>
          </div>
        </Modal>
      )}

      {caseDetails && (
        <Modal
          header="Case Details"
          showModal={showModal}
          setShowModal={setShowModel}
        >
          <div className="info">
            <p className="case_id">Case ID: {caseDetails.id}</p>
            <p>Client: {caseDetails.client}</p>
            <p>Issued Date: {caseDetails.issuedDate}</p>
          </div>

          <div className="actions">
            <span className="reassign">
              {caseDetails.assigned ? "Re-assign" : "Assign"}
            </span>
            <span className="delete">Delete</span>
          </div>
        </Modal>
      )}
    </div>
  );
};

export default Cases;
