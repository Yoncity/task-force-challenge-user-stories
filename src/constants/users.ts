type UserModel = {
    email: string,
    password: string,
    userRole: string
}

const users : Array<UserModel> = [
    {
        email: "yoni@gmail.com",
        password: "test1234",
        userRole: "secretary"
    },
    {
        email: "egin@gmail.com",
        password: "1234",
        userRole: "manager"
    },
    {
        email: "amani@gmail.com",
        password: "test",
        userRole: "advocator"
    },
];

export default users;