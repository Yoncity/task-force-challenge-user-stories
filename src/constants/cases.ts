export type CaseModel = {
  id: string;
  title: string;
  description: string;
  client: string;
  issuedDate: string;
  assigned?: boolean;
};

const cases: Array<CaseModel> = [
  {
    id: "#345",
    title: "Lorem Ipsum Dolor Imet",
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis culpa ad dolore quod labore quibusdam, enim deserunt nisi officiis asperiores sunt. Unde sit quae nisi officia est. Aliquam, minima voluptatem?",
    client: "court",
    issuedDate: new Date().toUTCString(),
    assigned: true,
  },

  {
    id: "#362",
    title: "Lorem Ipsum Dolor Imet",
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis culpa ad dolore quod labore quibusdam, enim deserunt nisi officiis asperiores sunt. Unde sit quae nisi officia est. Aliquam, minima voluptatem?",
    client: "court",
    issuedDate: new Date().toUTCString(),
  },

  {
    id: "#377",
    title: "Lorem Ipsum Dolor Imet",
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis culpa ad dolore quod labore quibusdam, enim deserunt nisi officiis asperiores sunt. Unde sit quae nisi officia est. Aliquam, minima voluptatem?",
    client: "court",
    issuedDate: new Date().toUTCString(),
    assigned: true,
  },
];

export default cases;
