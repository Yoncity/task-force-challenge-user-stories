export type InvoiceModel = {
  no: string;
  caseID: string;
  amount: string;
  issueDate: string;
  issuedBy: string;
  approved?: boolean;
};

const invoices: Array<InvoiceModel> = [
  {
    no: "34521",
    caseID: "#345",
    amount: "3000",
    issueDate: new Date().toUTCString(),
    issuedBy: "Yonatan Dawit",
    approved: true,
  },

  {
    no: "34534",
    caseID: "#377",
    amount: "1500",
    issueDate: new Date().toUTCString(),
    issuedBy: "Dawit Berhane",
  },
];

export default invoices;
