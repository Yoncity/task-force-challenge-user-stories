export type Locales = {
  pageNotFound: string;
};

const eng: Locales = {
  pageNotFound: "Page Not Found",
};

const languages = {
  eng,
};

export default languages;
