import Analytics from "../../components/Analytics";
import Cases from "../../components/Cases";
import AssignedCases from "../../components/Cases/AssignedCases";
import Header from "../../components/Header";
import { Redirect } from "react-router-dom";

const Home = (props) => {
  console.log("🚀 --- Home --- props", props);
  if (!props.location.state) return <Redirect to="/login" />;

  const {
    location: {
      state: {
        user: { userRole: role },
      },
    },
  } = props;

  return (
    <div className="home_container">
      <Header role={role} />
      {role === "advocator" && <AssignedCases />}
      {role === "manager" && <Cases />}
      {role === "secretary" && <Analytics />}
    </div>
  );
};

export default Home;
