import "./index.scss";
import { useHistory } from "react-router-dom";
import users from "../../constants/users";
import { useState } from "react";

const Login = () => {
  const { push } = useHistory();
  const [fields, setFields] = useState({ email: "", password: "" });
  const [error, setError] = useState("");

  const handleChange = ({ target: { name, value } }) => {
    setFields({ ...fields, [name]: value });
  };

  const validate = () => {
    if (!fields.email) {
      setError("Plase Provide Email Address");
      return;
    }
    if (!fields.password) {
      setError("Plase Provide Password");
      return;
    }
    const user = users.find(
      (item) => item.email === fields.email && item.password === fields.password
    );
    if (user) {
      push({
        pathname: "/",
        state: { user },
      });
      setError("");
    } else setError("Email and Password don't match");
  };

  return (
    <div className="login_container">
      <div className="login_container__left">
        <p className="login_container__left__title">Yombi</p>
        <p className="login_container__left__description">
          Secure All-in-one law firm management software.
        </p>
      </div>

      <div className="login_container__right">
        <p className="login_container__right__title">Login</p>

        <div className="login_container__right__forms">
          {error && (
            <p className="login_container__right__forms__error_message">
              {error}
            </p>
          )}
          <input
            onChange={handleChange}
            type="email"
            name="email"
            placeholder="Email Address"
          />
          <input
            onChange={handleChange}
            type="password"
            name="password"
            placeholder="Password"
          />
          <input type="submit" value="Submit" onClick={validate} />
        </div>
      </div>
    </div>
  );
};

export default Login;
